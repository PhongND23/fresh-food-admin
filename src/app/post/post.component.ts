import { Component, OnInit } from '@angular/core';
import { PostService } from '../service/postservice';
import { Message } from 'src/app/message/message';
import { PostCategoryService } from '../service/postcategoryservice';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ExcelService } from 'src/app/service/excel.service';
@Component({
    templateUrl: './post.component.html',
    providers: [MessageService, ConfirmationService, Message, ExcelService],
    styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
    public Editor = ClassicEditor;

    productDialog: boolean;

    dialogEdit: boolean = false;

    dialogDetail: boolean = false;

    deleteProductDialog: boolean = false;

    products: any[] = [];

    product: any = {};

    submitted: boolean;

    cols: any[];

    categories: any[] = [];

    rowsPerPageOptions = [5, 10, 20];

    name: string = '';
    categoryId: string = '';
    page: number = 0;
    size: number = 5;
    statusList: any = [];
    totalPages: number = 0;
    items:any=[]
    home:any={}
    constructor(
        private postService: PostService,
        private categoryService: PostCategoryService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private message: Message,
        private excel: ExcelService
    ) {}

    ngOnInit() {
        this.findAll();
        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'price', header: 'Price' },
            { field: 'category', header: 'Category' },
            { field: 'inventoryStatus', header: 'Status' },
        ];

        this.categoryService.drop().subscribe((data: any) => {
            this.categories = [];
            this.categories.push(...data);
        });

        this.statusList = [
            { lable: 'Hiển thị', value: true },
            { lable: 'Ẩn', value: false },
        ];
        this.product.content = '';
        this.items = [{ label: 'Quản lý' }, { label: 'Bài viết' }];
        this.home = { icon: 'pi pi-home', routerLink: '/main/dashboard' };
    }

    findAll() {
        this.postService
            .findAll(
                this.name,
                this.categoryId === null ? '' : this.categoryId,
                this.page,
                this.size
            )
            .subscribe((data: any) => {
                this.products = [];
                this.products = data.content;
                this.totalPages = data.totalPages;
            });
    }

    openNew() {
        this.product = {};
        this.product.status = true;
        this.submitted = false;
        this.productDialog = true;
        this.images = [];
    }

    showEdit(product: any) {
        this.product = { ...product };
        this.dialogEdit = true;
    }

    showDelete(product: any) {
        this.product = { ...product };
        this.deleteProductDialog = true;
    }

    confirmDelete() {
        this.postService.delete(this.product.id).subscribe(
            (data: any) => {
                this.products = this.products.filter(
                    (val) => val.id !== this.product.id
                );
                this.deleteProductDialog = false;
                this.message.success('d');
                this.product = {};
            },
            (error: any) => {
                if (error.status === 400) {
                    this.message.errorText(error.error.message);
                } else {
                    this.message.errorText('Server không phản hồi');
                }
            }
        );
    }

    hideDialog() {
        this.productDialog = false;
        this.dialogDetail = false;
        this.dialogEdit = false;
        this.deleteProductDialog = false;
        this.submitted = false;
    }

    saveProduct() {
        console.log(this.product);
        this.submitted = true;
        if (
            this.product.title &&
            this.product.categoryId &&
            this.images.length &&
            this.product.content
        ) {
            this.product.images = this.images;
            this.postService.save(this.product).subscribe(
                (res: any) => {
                    this.findAll();
                    this.product = {};
                    this.message.success('s');
                    this.productDialog = false;
                },
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    } else {
                        this.message.errorText('Server không phản hồi');
                    }
                }
            );
        } else {
            this.message.errorForm();
        }
    }

    updateProduct() {
        this.submitted = true;
        if (
            this.product.title &&
            this.product.categoryId &&
            this.product.images.length &&
            this.product.content
        ) {
            this.postService.edit(this.product.id, this.product).subscribe(
                (res: any) => {
                    this.product = {};
                    this.message.success('e');
                    this.dialogEdit = false;
                    this.findAll();
                },
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    } else {
                        this.message.errorText('Server không phản hồi');
                    }
                }
            );
        } else {
            this.message.errorForm();
        }
    }

    findIndexById(id: string): number {
        let index = -1;
        for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    createId(): string {
        let id = '';
        const chars =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    detail(product: any) {
        this.product = product;
        console.log(this.product);

        this.dialogDetail = true;
    }

    images: any[] = [];

    myUploader(event) {
        for (let file of event.target.files) {
            this.handleFileSelect(file);
        }
    }

    handleFileSelect(file: any) {
        if (file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    _handleReaderLoaded(readerEvt) {
        var base64textString = '';
        var binaryString = readerEvt.target.result;
        base64textString = 'data:image/jpg;base64,' + btoa(binaryString);
        this.images.push(base64textString);
    }

    updateImage(event) {
        for (let file of event.target.files) {
            this.handleFileSelectUpdate(file);
        }
    }

    handleFileSelectUpdate(file: any) {
        if (file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoadedUpdate.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    _handleReaderLoadedUpdate(readerEvt) {
        var base64textString = '';
        var binaryString = readerEvt.target.result;
        base64textString = 'data:image/jpg;base64,' + btoa(binaryString);
        this.product.images.push(base64textString);
    }

    removeImage(file, router: boolean) {
        var index = -1;
        if (router) {
            index = this.images.indexOf(file);
            if (index !== -1) {
                this.images.splice(index, 1);
            }
        } else {
            index = this.product.images.indexOf(file);
            if (index !== -1) {
                this.product.images.splice(index, 1);
            }
        }
    }

    clearArr(isAdd: boolean) {
        if (isAdd) {
            this.images = [];
        } else {
            this.product.images = [];
        }
    }

    search() {
        this.page = 0;
        this.size = 5;
        this.findAll();
    }

    nextPage() {
        if (this.page + 1 < this.totalPages) {
            if (this.categoryId === null) this.categoryId = '';
            this.page += 1;
            this.findAll();
        }
    }

    previousPage() {
        if (this.page - 1 >= 0) {
            if (this.categoryId === null) this.categoryId = '';
            this.page -= 1;
            this.findAll();
        }
    }

    exportExcel() {
        var list = [
            {
                STT: 0,
                Mã_sản_phẩm: '',
                Tên_sản_phẩm: '',
                Danh_mục: '',
                Giá: '',
                Trạng_thái: '',
            },
        ];

        for (let index = 0; index < this.products.length; index++) {
            const element = this.products[index];
            var item = {
                STT: index + 1,
                Mã_sản_phẩm: element.id,
                Tên_sản_phẩm: element.name,
                Danh_mục: element.categoryName,
                Giá: element.price,
                Trạng_thái: element.status ? 'Hoạt động' : 'Ẩn',
            };
            list.push(item);
        }
        list.shift();
        this.excel.exportAsExcelFile(list, 'Danh sách sản phẩm');
    }

    clearFilter() {
        this.name = '';
        this.categoryId = '';
        this.page = 0;
        this.size = 5;
        this.findAll();
    }
}
