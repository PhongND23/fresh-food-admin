import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class PostCategoryService {
    constructor(private http: HttpClient) {}

    findAll(name: any, page: any, size: any): any {
        return this.http.get(
            `${environment.url}/post-category?name=${name}&page=${page}&size=${size}&sort=createdDate,desc`
        );
    }

    save(dto: any) {
        return this.http.post(`${environment.url}/post-category`, dto);
    }

    edit(id: any, dto: any): any {
        return this.http.put(`${environment.url}/post-category/${id}`, dto);
    }

    delete(id: any): any {
        return this.http.delete(`${environment.url}/post-category/${id}`);
    }
    drop():any
    {
        return this.http.get(`${environment.url}/post-category/drop`);
    }
}
