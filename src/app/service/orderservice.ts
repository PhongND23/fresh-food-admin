import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class OrderService {
    constructor(private http: HttpClient) {}

    findAll(dto: any,page:any,size:any): any {
        return this.http.post(`${environment.url}/order/all?page=${page}&size=${size}`, dto);
    }
    change(id: any,statusId:any) {
        return this.http.get(`${environment.url}/order/change/${id}/${statusId}`);
    }
    dropStatus() {
        return this.http.get(`${environment.url}/order-status/drop`);
    }
}
