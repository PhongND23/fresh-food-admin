import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private http: HttpClient) {}

  findAll(productId: any, postId: any,status:any, page: any, size: any): any {
    return this.http.get(
      `${environment.url}/comment?productId=${productId}&postId=${postId}&status=${status}&page=${page}&size=${size}&sort=postedDate,desc`
    );
  }

  update(id: any,status:any) {
    return this.http.get(`${environment.url}/comment/${id}?status=${status}`);
  }
}
