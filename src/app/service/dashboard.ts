import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  getOverview() {
    return this.http.get(`${environment.url}/dashboard`);
  }

  getOverviewById(id:any) {
    return this.http.get(`${environment.url}/dashboard?accountId=${id}`);
  }

  getTotalMonth(id:any) {
    return this.http.get(`${environment.url}/dashboard/total-month/${id}`);
  }

  getTop(id:any) {
    return this.http.get(`${environment.url}/dashboard/top/${id}`);
  }
}
