import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class ProductService {


    constructor(private http: HttpClient) {}

    findAll(name: any,categoryId:any,accountId:any, page: any, size: any): any {
        return this.http.get(
            `${environment.url}/product?name=${name}&categoryId=${categoryId}&accountId=${accountId}&page=${page}&size=${size}&sort=createdDate,desc`
        );
    }

    save(dto: any) {
        return this.http.post(`${environment.url}/product`, dto);
    }

    edit(id: any, dto: any): any {
        return this.http.put(`${environment.url}/product/${id}`, dto);
    }

    delete(id: any): any {
        return this.http.delete(`${environment.url}/product/${id}`);
    }
    getProductsSmall() {

    }
}

