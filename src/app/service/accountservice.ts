import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class AccountService {


    constructor(private http: HttpClient) {}

    findAll(username: any,roleId:any, page: any, size: any): any {
        return this.http.get(
            `${environment.url}/account?username=${username}&roleId=${roleId}&page=${page}&size=${size}&sort=createdDate,desc`
        );
    }

    save(dto: any) {
        return this.http.post(`${environment.url}/account`, dto);
    }

    edit(id: any, dto: any): any {
        return this.http.put(`${environment.url}/account/${id}`, dto);
    }

    delete(id: any): any {
        return this.http.delete(`${environment.url}/account/${id}`);
    }
    drop() {
        return this.http.get(`${environment.url}/role/drop`)
    }
    login(dto:any)
    {
        return this.http.post(`${environment.url}/account/login`,dto)
    }
}

