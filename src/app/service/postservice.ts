import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
    providedIn: 'root',
})
export class PostService {


    constructor(private http: HttpClient) {}

    findAll(name: any,categoryId:any, page: any, size: any): any {
        return this.http.get(
            `${environment.url}/post?title=${name}&categoryId=${categoryId}&page=${page}&size=${size}&sort=createdDate,desc`
        );
    }

    save(dto: any) {
        return this.http.post(`${environment.url}/post`, dto);
    }

    edit(id: any, dto: any): any {
        return this.http.put(`${environment.url}/post/${id}`, dto);
    }

    delete(id: any): any {
        return this.http.delete(`${environment.url}/post/${id}`);
    }
    getpostsSmall() {

    }
}

