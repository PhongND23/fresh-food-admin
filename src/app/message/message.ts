import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class Message {
    constructor(private message: MessageService) {}

    success(type: any) {
        var detail = '';
        switch (type) {
            case 's':
                detail = 'Tạo mới thành công';
                break;
            case 'e':
                detail = 'Cập nhật thành công';
                break;
            case 'd':
                detail = 'Xóa thành công';
                break;
            default:
                detail = 'Thao tác thành công';
                break;
        }
        this.message.add({
            severity: 'success',
            summary: 'Thông báo',
            detail: detail,
            life: 2500,
        });
    }

    errorForm() {
        this.message.add({
            severity: 'error',
            summary: 'Lỗi',
            detail: 'Dữ liệu không hợp lệ',
            life: 2500,
        });
    }

    errorText(text:any) {
        this.message.add({
            severity: 'error',
            summary: 'Lỗi',
            detail: text,
            life: 2500,
        });
    }

    hidden() {
        this.message.add({
            severity: 'success',
            summary: 'Thông báo',
            detail: 'Đã ẩn bản ghi',
            life: 2500,
        });
    }
    warning(text:any)
    {
        this.message.add({
            severity: 'warn',
            summary: 'Chú ý',
            detail: text,
            life: 2500,
        });
    }
}
