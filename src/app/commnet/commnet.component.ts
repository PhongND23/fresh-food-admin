import { Component, OnInit } from '@angular/core';
import { CommentService } from '../service/commentservice';
import { Message } from 'src/app/message/message';
import { PostCategoryService } from '../service/postcategoryservice';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ExcelService } from 'src/app/service/excel.service';
@Component({
    templateUrl: './commnet.component.html',
    providers: [MessageService, ConfirmationService, Message, ExcelService],
    styleUrls: ['./commnet.component.scss'],
})
export class CommnetComponent implements OnInit {
    public Editor = ClassicEditor;

    productDialog: boolean;

    dialogEdit: boolean = false;

    dialogDetail: boolean = false;

    deleteProductDialog: boolean = false;

    products: any[] = [];

    product: any = {};

    submitted: boolean;

    cols: any[];

    categories: any[] = [];

    rowsPerPageOptions = [5, 10, 20];

    name: string = '';
    categoryId: string = '';
    page: number = 0;
    size: number = 5;
    statusList: any = [];
    totalPages: number = 0;
    items: any = [];
    home: any;
    status: any = '';
    constructor(
        private commentService: CommentService,
        private categoryService: PostCategoryService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private message: Message,
        private excel: ExcelService
    ) {}

    ngOnInit() {
        this.findAll();
        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'price', header: 'Price' },
            { field: 'category', header: 'Category' },
            { field: 'inventoryStatus', header: 'Status' },
        ];

        this.categoryService.drop().subscribe((data: any) => {
            this.categories = [];
            this.categories.push(...data);
        });

        this.statusList = [
            { lable: 'Hiển thị', value: true },
            { lable: 'Ẩn', value: false },
        ];
        this.product.content = '';
        this.items = [{ label: 'Quản lý' }, { label: 'Bình luận' }];
        this.home = { icon: 'pi pi-home', routerLink: '/main/dashboard' };
    }

    findAll() {
        this.commentService
            .findAll('', '', this.status, this.page, this.size)
            .subscribe((data: any) => {
                this.products = [];
                this.products = data.content;
                this.totalPages = data.totalPages;
            });
    }

    openNew() {
        this.product = {};
        this.product.status = true;
        this.submitted = false;
        this.productDialog = true;
    }

    showEdit(product: any) {
        this.product = { ...product };
        this.dialogEdit = true;
    }

    showDelete(product: any) {
        this.product = { ...product };
        this.deleteProductDialog = true;
    }

    confirmDelete() {
        this.commentService
            .update(this.product.id, this.product.status ? false : true)
            .subscribe(
                (data: any) => {
                    this.message.success('e');
                    this.product = {};
                    this.hideDialog();
                    this.findAll();
                },
                (error: any) => {
                    if (error.status === 400) {
                        this.message.errorText(error.error.message);
                    } else {
                        this.message.errorText('Server không phản hồi');
                    }
                }
            );
    }

    hideDialog() {
        this.productDialog = false;
        this.dialogDetail = false;
        this.dialogEdit = false;
        this.deleteProductDialog = false;
        this.submitted = false;
    }

    detail(product: any) {
        this.product = product;
        console.log(this.product);

        this.dialogDetail = true;
    }

    search() {
        this.page = 0;
        this.size = 5;
        this.findAll();
    }

    nextPage() {
        if (this.page + 1 < this.totalPages) {
            if (this.categoryId === null) this.categoryId = '';
            this.page += 1;
            this.findAll();
        }
    }

    previousPage() {
        if (this.page - 1 >= 0) {
            if (this.categoryId === null) this.categoryId = '';
            this.page -= 1;
            this.findAll();
        }
    }

    clearFilter() {
        this.status = '';
        this.findAll();
    }
}
