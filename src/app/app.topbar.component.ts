import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';
import { Subscription } from 'rxjs';
import { MenuItem } from 'primeng/api';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    providers: [ConfirmationService, MessageService],
})
export class AppTopBarComponent implements OnInit {

    account: any = {};
    dialog: boolean = false;
    user:any={}
    constructor(
        public appMain: AppMainComponent,
        private confirm: ConfirmationService,
        private mess: MessageService,
        private router: Router
    ) {}
    ngOnInit(): void {
        this.account = JSON.parse(localStorage.getItem('account'));
    }
    open() {
        this.dialog = true;
    }
    logout() {
        this.dialog = false;
        localStorage.removeItem('account');
        this.router.navigate(['/']);
    }
}
