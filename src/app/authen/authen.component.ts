import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../service/accountservice';
import { Message } from '../message/message';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
    templateUrl: './authen.component.html',
    providers: [ConfirmationService, MessageService, Message],
    styles: [
        `
            :host ::ng-deep .p-password input {
                width: 100%;
                padding: 1rem;
            }

            :host ::ng-deep .pi-eye {
                transform: scale(1.6);
                margin-right: 1rem;
                color: var(--primary-color) !important;
            }

            :host ::ng-deep .pi-eye-slash {
                transform: scale(1.6);
                margin-right: 1rem;
                color: var(--primary-color) !important;
            }
        `,
    ],
})
export class AuthenComponent implements OnInit {
    valCheck: string[] = ['remember'];

    password: string;

    login: any = {};

    constructor(
        private router: Router,
        private accountService: AccountService,
        private mess: MessageService,
        private confirm: ConfirmationService,
        private message: Message
    ) {}

    ngOnInit(): void {}

    authen() {
        if (this.login.userName && this.login.password) {
            this.login.loginSystem = true;
            console.log(this.login);
            this.accountService.login(this.login).subscribe(
                (data: any) => {
                    data.password=''
                    localStorage.setItem('account',JSON.stringify(data))
                    this.router.navigate(['/main/dashboard']);
                },
                (error: any) => {
                    if (error.status === 400) {
                        this.message.errorText(error.error.message);
                    }
                }
            );
        }else
        {
            this.message.warning('Tên tài khoản và mật khẩu không được để trống')
        }
    }
}
