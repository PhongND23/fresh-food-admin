import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ProductCategoryService } from '../service/productcategoryservice';
import { Message } from '../message/message';
@Component({
    templateUrl: './product-category.component.html',
    providers: [MessageService, ConfirmationService, Message],
    styleUrls: ['./product-category.component.scss'],
})
export class ProductCategoryComponent implements OnInit {
    productDialog: boolean;

    dialogEdit: boolean = false;

    dialogDetail: boolean = false;

    deleteProductDialog: boolean = false;

    deleteProductsDialog: boolean = false;

    category: any = {};

    submitted: boolean;

    cols: any[];

    rowsPerPageOptions = [5, 10, 20];

    value: number = 0;

    categories: any[] = [];

    keySearch: string = 'llll';
    count = 0;
    statusList: any[];
    first = 0;
    rows = 1;
    page: number = 0;
    size: number = 5;
    name: string = '';
    totalPages: number = 0;
    public uploadedFiles: any = [];
    items:any=[]
    home:any={}
    constructor(
        private categoryService: ProductCategoryService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private message: Message
    ) {}

    ngOnInit() {
        this.findAll();
        this.cols = [
            { field: 'order', header: 'order' },
            { field: 'code', header: 'code' },
            { field: 'name', header: 'name' },
            { field: 'status', header: 'status' },
        ];
        this.statusList = [
            { lable: 'Hoạt động', value: true },
            { lable: 'Nháp', value: false },
        ];
        this.items = [{ label: 'Quản lý' }, { label: 'Danh mục sản phẩm' }];
        this.home = { icon: 'pi pi-home', routerLink: '/main/dashboard' };
    }

    openNew() {
        this.category = {};
        this.category.status = true;
        this.submitted = false;
        this.productDialog = true;
    }
    findAll() {
        this.categories = [];
        this.categoryService
            .findAll(this.name, this.page, this.size)
            .subscribe((data: any) => {
                this.categories.push(...data.content);
                this.totalPages = data.totalPages;
            });
    }

    showEdit(category: any) {
        this.category = { ...category };
        this.dialogEdit = true;
    }
    edit(category: any) {
        this.submitted = true;
        if (category.name) {
            this.categoryService
                .edit(category.id, category)
                .subscribe((res: any) => {
                    this.findAll();
                    this.message.success('e');
                    this.dialogEdit = false;
                    this.category = {};
                }),
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    }
                };
        } else {
            this.message.errorForm();
        }
    }

    showDelete(category: any) {
        this.deleteProductDialog = true;
        this.category = category;
    }

    confirmDelete() {
        this.categoryService.delete(this.category.id).subscribe(
            (res: any) => {
                this.findAll();
                this.message.success('s');
            },
            (error: any) => {
                if (error.status === 400) {
                    this.message.errorText(error.error.message);
                } else {
                    this.message.errorText('Server không phản hồi');
                }
            }
        );
        this.category = {};
        this.deleteProductDialog = false;
    }

    hideDialog() {
        this.productDialog = false;
        this.dialogDetail = false;
        this.dialogEdit = false;
        this.submitted = false;
    }

    // findIndexById(id: string): number {
    //     let index = -1;
    //     for (let i = 0; i < this.products.length; i++) {
    //         if (this.products[i].id === id) {
    //             index = i;
    //             break;
    //         }
    //     }

    //     return index;
    // }

    status: any = {};
    save() {
        this.submitted = true;
        if (this.category.name) {
            this.categoryService.save(this.category).subscribe(
                (res: any) => {
                    this.page = 0;
                    this.size = 5;
                    this.findAll();
                    this.category = {};
                    this.message.success('s');
                },
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    }
                }
            );
            this.productDialog = false;
        } else {
            this.message.errorForm();
        }
    }

    detail(category: any) {
        this.category = category;
        this.dialogDetail = true;
        console.log(this.category);
    }

    nextPage() {
        if (this.page + 1 < this.totalPages) {
            this.page += 1;
            this.findAll();
        }
    }

    previousPage() {
        if (this.page - 1 >= 0) {
            this.page -= 1;
            this.findAll();
        }
    }
    search() {
        this.page = 0;
        this.size = 5;
        this.findAll();
    }
    clearFilter() {
        this.name = '';
        this.findAll();
    }
}
