import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Product } from '../../api/product';
import { ProductService } from 'src/app/service/productservice';
import { DashboardService } from 'src/app/service/dashboard';
import { Subscription } from 'rxjs';
import { ConfigService } from '../../service/app.config.service';
import { AppConfig } from '../../api/appconfig';
import { AccountService } from 'src/app/service/accountservice';
@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    items: MenuItem[];

    products: Product[];

    chartData: any;

    chartOptions: any;

    subscription: Subscription;

    config: AppConfig;
    over: any = {};
    account:any={}
    tops:any=[]
    total:any=0;
    constructor(
        private productService: ProductService,
        public configService: ConfigService,
        private dashboard: DashboardService,
        private accountService:AccountService
    ) {}

    ngOnInit() {
        this.config = this.configService.config;
        this.subscription = this.configService.configUpdate$.subscribe(
            (config) => {
                this.config = config;
                this.updateChartOptions();
            }
        );
        this.productService.getProductsSmall();

        this.items = [
            { label: 'Add New', icon: 'pi pi-fw pi-plus' },
            { label: 'Remove', icon: 'pi pi-fw pi-minus' },
        ];

        this.chartData = {
            labels: [
                'Tháng 1',
                'Tháng 2',
                'Tháng 3',
                'Tháng 4',
                'Tháng 5',
                'Tháng 6',
                'Tháng 7',
                'Tháng 8',
                'Tháng 9'
            ],
            datasets: [
                {
                    label: 'Lượt khách hàng truy cập',
                    data: [40, 55, 60, 75, 89, 95, 99,85,100],
                    fill: false,
                    backgroundColor: '#2f4860',
                    borderColor: '#2f4860',
                    tension: 0.4,
                },
                {
                    label: 'Lượt mua hàng',
                    data: [35, 50, 54, 68, 86, 80, 80,60,89],
                    fill: false,
                    backgroundColor: '#00bb7e',
                    borderColor: '#00bb7e',
                    tension: 0.4,
                },
            ],
        };

        var json=localStorage.getItem('account')
        this.account=!json?null:JSON.parse(json)
        console.log(this.account)
        if(this.account.id)
        {
            this.dashboard.getOverviewById(this.account.id).subscribe((res: any) => {
                this.over = res;
            });
        }
        else
        {
            this.dashboard.getOverview().subscribe((res: any) => {
                this.over = res;
            });
        }


        this.dashboard.getTop(this.account.id).subscribe((res:any)=>{
            this.tops=res;
        })
        this.dashboard.getTotalMonth(this.account.id).subscribe((res:any)=>{
            this.total=res;
        })
    }

    updateChartOptions() {
        if (this.config.dark) this.applyDarkTheme();
        else this.applyLightTheme();
    }

    applyDarkTheme() {
        this.chartOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#ebedef',
                    },
                },
            },
            scales: {
                x: {
                    ticks: {
                        color: '#ebedef',
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    },
                },
                y: {
                    ticks: {
                        color: '#ebedef',
                    },
                    grid: {
                        color: 'rgba(160, 167, 181, .3)',
                    },
                },
            },
        };
    }

    applyLightTheme() {
        this.chartOptions = {
            plugins: {
                legend: {
                    labels: {
                        color: '#495057',
                    },
                },
            },
            scales: {
                x: {
                    ticks: {
                        color: '#495057',
                    },
                    grid: {
                        color: '#ebedef',
                    },
                },
                y: {
                    ticks: {
                        color: '#495057',
                    },
                    grid: {
                        color: '#ebedef',
                    },
                },
            },
        };
    };


}
