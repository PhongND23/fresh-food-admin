import { Component, OnInit } from '@angular/core';
import { Product } from '../../api/product';
import { ProductService } from 'src/app/service/productservice';
import { Message } from 'src/app/message/message';
import { ProductCategoryService } from 'src/app/service/productcategoryservice';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ExcelService } from 'src/app/service/excel.service';
@Component({
    templateUrl: './products.component.html',
    providers: [MessageService, ConfirmationService, Message, ExcelService],
    styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
    public Editor = ClassicEditor;

    productDialog: boolean;

    dialogEdit: boolean = false;

    dialogDetail: boolean = false;

    deleteProductDialog: boolean = false;

    products: any[] = [];

    product: any = {};

    selectedProducts: Product[];

    submitted: boolean;

    cols: any[];

    categories: any[] = [];

    rowsPerPageOptions = [5, 10, 20];

    name: string = '';
    categoryId: string = '';
    page: number = 0;
    size: number = 5;
    statusList: any = [];
    totalPages: number = 0;
    items: any = [];
    home: any;
    accountId: any;
    account: any = {};
    constructor(
        private productService: ProductService,
        private categoryService: ProductCategoryService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private message: Message,
        private excel: ExcelService
    ) {}

    ngOnInit() {
        this.findAll();
        this.items = [{ label: 'Quản lý' }, { label: 'Sản phẩm' }];
        this.home = { icon: 'pi pi-home', routerLink: '/main/dashboard' };

        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'price', header: 'Price' },
            { field: 'category', header: 'Category' },
            { field: 'inventoryStatus', header: 'Status' },
        ];

        this.categoryService.drop().subscribe((data: any) => {
            this.categories = [];
            this.categories.push(...data);
        });

        this.statusList = [
            { lable: 'Hoạt động', value: true },
            { lable: 'Ẩn', value: false },
        ];
    }

    findAll() {
        this.account = JSON.parse(localStorage.getItem('account'));
        this.productService
            .findAll(
                this.name,
                this.categoryId === null ? '' : this.categoryId,
                this.account.roleId === 1 ? '' : this.account.id,
                this.page,
                this.size
            )
            .subscribe((data: any) => {
                this.products = [];
                this.products = data.content;
                this.totalPages = data.totalPages;
            });
    }

    openNew() {
        this.product = {};
        this.product.status = true;
        this.submitted = false;
        this.productDialog = true;
        this.images = [];
    }

    showEdit(product: any) {
        this.product = { ...product };
        this.dialogEdit = true;
    }

    showDelete(product: any) {
        this.product = { ...product };
        this.deleteProductDialog = true;
    }

    deleteProduct(product: Product) {
        this.deleteProductDialog = true;
        this.product = product;
    }

    confirmDelete() {
        this.productService.delete(this.product.id).subscribe(
            (data: any) => {
                this.products = this.products.filter(
                    (val) => val.id !== this.product.id
                );
                this.deleteProductDialog = false;
                this.message.success('d');
                this.product = {};
            },
            (error: any) => {
                if (error.status === 400) {
                    this.message.errorText(error.error.message);
                } else {
                    this.message.errorText('Server không phản hồi');
                }
            }
        );
    }

    hideDialog() {
        this.productDialog = false;
        this.dialogDetail = false;
        this.dialogEdit = false;
        this.deleteProductDialog = false;
        this.submitted = false;
    }

    saveProduct() {
        this.submitted = true;
        if (
            this.product.name &&
            this.product.categoryId &&
            this.images.length &&
            this.product.price &&
            this.product.promotionPrice &&
            this.product.quantity &&
            this.product.unit &&
            this.product.shortDescription &&
            this.product.description &&
            this.product.promotionPrice < this.product.price
        ) {
            this.product.images = this.images;
            this.product.accountId = this.account.id;
            this.productService.save(this.product).subscribe(
                (res: any) => {
                    this.findAll();
                    this.product = {};
                    this.message.success('s');
                    this.productDialog = false;
                },
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    } else {
                        this.message.errorText('Server không phản hồi');
                    }
                }
            );
        } else {
            this.message.errorForm();
        }
    }

    updateProduct() {
        console.log(this.product);
        this.submitted = true;
        if (
            this.product.name &&
            this.product.categoryId &&
            this.product.images.length &&
            this.product.price &&
            this.product.promotionPrice &&
            this.product.quantity &&
            this.product.unit &&
            this.product.shortDescription &&
            this.product.description &&
            this.product.promotionPrice < this.product.price
        ) {
            this.productService.edit(this.product.id, this.product).subscribe(
                (res: any) => {
                    this.product = {};
                    this.message.success('e');
                    this.dialogEdit = false;
                    this.findAll();
                },
                (error: any) => {
                    if (error.status === 400 || error.status === 500) {
                        this.message.errorForm();
                    } else {
                        this.message.errorText('Server không phản hồi');
                    }
                }
            );
        } else {
            this.message.errorForm();
        }
    }

    findIndexById(id: string): number {
        let index = -1;
        for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    createId(): string {
        let id = '';
        const chars =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    detail(product: any) {
        this.product = product;
        console.log(this.product);

        this.dialogDetail = true;
    }

    images: any[] = [];

    updateImage(event) {
        for (let file of event.target.files) {
            this.handleFileSelectUpdate(file);
        }
    }

    handleFileSelectUpdate(file: any) {
        if (file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoadedUpdate.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    _handleReaderLoadedUpdate(readerEvt) {
        var base64textString = '';
        var binaryString = readerEvt.target.result;
        base64textString = 'data:image/jpg;base64,' + btoa(binaryString);
        this.product.images.push(base64textString);
    }

    removeImage(file, router: boolean) {
        var index = -1;
        if (router) {
            index = this.images.indexOf(file);
            if (index !== -1) {
                this.images.splice(index, 1);
            }
        } else {
            index = this.product.images.indexOf(file);
            if (index !== -1) {
                this.product.images.splice(index, 1);
            }
        }
    }

    clearArr(isAdd: boolean) {
        if (isAdd) {
            this.images = [];
        } else {
            this.product.images = [];
        }
    }

    search() {
        this.page = 0;
        this.size = 5;
        this.findAll();
    }

    nextPage() {
        if (this.page + 1 < this.totalPages) {
            if (this.categoryId === null) this.categoryId = '';
            this.page += 1;
            this.findAll();
        }
    }

    previousPage() {
        if (this.page - 1 >= 0) {
            if (this.categoryId === null) this.categoryId = '';
            this.page -= 1;
            this.findAll();
        }
    }

    exportExcel() {
        var list = [
            {
                STT: 0,
                Mã_sản_phẩm: '',
                Tên_sản_phẩm: '',
                Danh_mục: '',
                Giá: '',
                Trạng_thái: '',
            },
        ];

        for (let index = 0; index < this.products.length; index++) {
            const element = this.products[index];
            var item = {
                STT: index + 1,
                Mã_sản_phẩm: element.id,
                Tên_sản_phẩm: element.name,
                Danh_mục: element.categoryName,
                Giá: element.price,
                Trạng_thái: element.status ? 'Hoạt động' : 'Ẩn',
            };
            list.push(item);
        }
        list.shift();
        this.excel.exportAsExcelFile(list, 'Danh sách sản phẩm');
    }

    clearFilter() {
        this.name = '';
        this.categoryId = '';
        this.page = 0;
        this.size = 5;
        this.findAll();
    }

    myUploader(event) {
        for (let file of event.target.files) {
            this.handleFileSelect(file);
        }
    }

    handleFileSelect(file: any) {
        if (file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    _handleReaderLoaded(readerEvt) {
        var base64textString = '';
        var binaryString = readerEvt.target.result;
        base64textString = 'data:image/jpg;base64,' + btoa(binaryString);
        this.images.push(base64textString);
    }
}
