import { Component, OnInit } from '@angular/core';
import { OrderService } from '../service/orderservice';
import { Message } from 'src/app/message/message';
import { ProductCategoryService } from 'src/app/service/productcategoryservice';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ExcelService } from 'src/app/service/excel.service';
@Component({
    templateUrl: './seller-order.component.html',
    providers: [MessageService, ConfirmationService, Message, ExcelService],
    styleUrls: ['./seller-order.component.scss'],
})
export class SellerOrderComponent implements OnInit {
    public Editor = ClassicEditor;

    productDialog: boolean;

    dialogEdit: boolean = false;

    dialogDetail: boolean = false;

    deleteProductDialog: boolean = false;

    products: any[] = [];

    product: any = {};

    submitted: boolean;

    cols: any[];

    categories: any[] = [];

    rowsPerPageOptions = [5, 10, 20];

    name: string = '';
    categoryId: string = '';
    page: number = 0;
    size: number = 5;
    statusList: any = [];
    totalPages: number = 0;
    items: any = [];
    home: any;
    accountId: any;
    account: any = {};
    searchDTO: any = {};
    statusDrop:any[]=[];
    statusId:string='';
    constructor(
        private orderService: OrderService,
        private categoryService: ProductCategoryService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private message: Message,
        private excel: ExcelService
    ) {}

    ngOnInit() {
        this.findAll();
        this.items = [{ label: 'Quản lý' }, { label: 'Đơn hàng' }];
        this.home = { icon: 'pi pi-home', routerLink: '/main/dashboard' };

        this.cols = [
            { field: 'name', header: 'Name' },
            { field: 'price', header: 'Price' },
            { field: 'category', header: 'Category' },
            { field: 'inventoryStatus', header: 'Status' },
        ];

        this.orderService.dropStatus().subscribe((res:any)=>{
            this.statusDrop.push({name:'Tất cả',id:''})
            this.statusDrop.push(...res)

        })

    }

    findAll() {
        this.account = JSON.parse(localStorage.getItem('account'));
        if (this.account.roleId === 2) {
            this.searchDTO.sellerId = this.account.id;
        }
        this.orderService
            .findAll(this.searchDTO, this.page, this.size)
            .subscribe((data: any) => {
                this.products = [];
                this.products = data.content;
                this.totalPages = data.totalPages;
            });
    }

    showEdit(product: any) {
        this.product = { ...product };
        this.dialogEdit = true;
    }

    hideDialog() {
        this.productDialog = false;
        this.dialogDetail = false;
        this.dialogEdit = false;
        this.deleteProductDialog = false;
        this.submitted = false;
    }



    detail(product: any) {
        this.product = product;
        console.log(this.product);
        this.dialogDetail = true;
    }





    search() {
        this.searchDTO.orderStatusId=this.statusId;
        this.searchDTO.id=this.name;
        this.findAll();
    }

    nextPage() {
        if (this.page + 1 < this.totalPages) {
            if (this.categoryId === null) this.categoryId = '';
            this.page += 1;
            this.findAll();
        }
    }

    previousPage() {
        if (this.page - 1 >= 0) {
            if (this.categoryId === null) this.categoryId = '';
            this.page -= 1;
            this.findAll();
        }
    }

    exportExcel() {
        var list = [
            {
                STT: 0,
                Mã_sản_phẩm: '',
                Tên_sản_phẩm: '',
                Danh_mục: '',
                Giá: '',
                Trạng_thái: '',
            },
        ];

        for (let index = 0; index < this.products.length; index++) {
            const element = this.products[index];
            var item = {
                STT: index + 1,
                Mã_sản_phẩm: element.id,
                Tên_sản_phẩm: element.name,
                Danh_mục: element.categoryName,
                Giá: element.price,
                Trạng_thái: element.status ? 'Hoạt động' : 'Ẩn',
            };
            list.push(item);
        }
        list.shift();
        this.excel.exportAsExcelFile(list, 'Danh sách sản phẩm');
    }

    clearFilter() {
        this.searchDTO.id = '';
        this.name = '';
        this.statusId = '';
        this.searchDTO.orderStatusId = '';
        this.findAll();
    }

    getTotal(list: any) {
        var total = 0;
        for (var item of list) {
            total += item.quantity + item.currentPrice;
        }
        return total-2;
    }

    changeStatus() {
        this.orderService
            .change(this.product.id, this.product.orderStatusId)
            .subscribe((res: any) => {
                this.message.success('e');
                this.dialogEdit = false;
                this.findAll();
            });
    }
}
